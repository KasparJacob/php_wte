<?php
require_once 'WordSection.php';

/* 
 * Kaspar Smoolenaars <kaspar.smoolenaars@gmail.com>
 * 
 * Class that handles the body section, handles special substitutions.
 * 
 * TODO: Make it be able to deal with tables.
 * TODO: So that the headers and footers remain but the datacells can be repeated.
 */

class WordDocument extends WordSection {
	const TABLE_REGEX			= '/<w:tbl.*?>(?P<table>.+?)<\/w:tbl>/';
	const TABLE_ROW_REGEX		= '/<w:tr.*?>(?P<row>.+?)<\/w:tr>/';
	const TABLE_COLUMN_REGEX	= '/<w:tc.*?>(?P<cell>)<\/w:tc>/';
	
	public function make( $keyhash, $regex=null ){
		$this->keyhash = ( is_array( $keyhash ) ? $keyhash : array() );
		$filecontent = file_get_contents( $this->filepath );
		$this->processTables( $filecontent );
		if( is_null( $regex ) ) {
			$regex = self::ANCHOR_REGEX;
		}
		$content = preg_replace_callback( $regex, array( $this, 'replace' ), $filecontent );
		unset( $filecontent );
		return ( file_put_contents( $this->filepath, $content ) !== false );
	}
	
	protected function replace( $matches ) {
		$label = self::lazyStrip( $matches['label'] );
		if( array_key_exists( $label, $this->keyhash ) ) {
			return $this->keyhash[$label];
		}
		return $matches['label'];
	}
	
	protected function processTables( &$filecontent , $regex=null ) {
		if( is_null( $regex ) ) {
			$regex = self::TABLE_REGEX;
		}
		$content = preg_replace_callback( $regex, array( $this, 'replacetable' ), $filecontent );
		return $content;
	}
	
	protected function replaceTable( $content ) {
		$regex = self::TABLE_ROW_REGEX;
		$content = preg_replace_callback( $regex, array( $this, 'replacerows' ), $content['table'] );
	}
	
	protected function replaceRows( $content ) {
		$regex = self::ANCHOR_REGEX;
		preg_match_all( $regex, $content['row'], $matches );
		foreach( $matches['label'] as $label ) {
			
		}
		echo '<pre>'. print_r( $matches ). '</pre>';
		//if( )
		//$content = preg_replace_callback( $regex, array( $this, 'replacecells' ), $content['row'] );
	}
}