<?php 
require_once 'WordTemplate.php';

$keyhash = array(	'headers'			=> array(	'header_1_anchor'	=> 'Frontpage header',
													'header_2_anchor'	=> 'Page header' ),
					'document'			=> array(	'person_title'		=> 'Mr.',
													'person_name'		=> 'Bob',
													'head_1_anchor'		=> 'We do',
													'head_2_anchor'		=> 'We don\'t',
													'head_3_anchor'		=> 'Still we do',
													'description'		=> 'Wooden ball, red.',
													'price'				=> '1,00',
													'number'			=> '5',
													'VAT_rate'			=> '19%',
													'netto'				=> '5.00',
													'bruto'				=> '5.95',
													'netto_total'		=> '5.00',	
													'bruto_total'		=> '5.95',
													'a_very_long_text'	=> 'Rubidium can be liquid at ambient temperature, but only on a hot day given that its melting point is about 40°C. It is a soft, silvery-white metallic element of the alkali metals group (Group 1). It is one of the most most electropositive and alkaline elements. It ignites spontaneously in air and reacts violently with water, setting fire to the liberated hydrogen. As so with all the other alkali metals, it forms amalgams with mercury. It alloys with gold, caesium, sodium, and potassium. It colours a flame yellowish-violet.

Science and Ink cartoon for rubidium
Cartoon by Nick D Kim ([Science and Ink], used by permission).

Name: rubidium
Symbol: Rb
Atomic number: 37
Relative atomic mass (Ar): 85.4678 (3) [see note g]
Standard state: solid at 298 K
Colour: silvery white
Classification: Metallic
Group in periodic table: 1
Group name: Alkali metal
Period in periodic table: 5
Block in periodic table: s-block
Electron shell structure: 2.8.18.8.1
CAS Registry ID: 7440-17-7
rubidium
Image adapted with permission from Prof James Marshall\'s (U. North Texas, USA) Walking Tour of the elements CD.

Rubidium: historical information
Rubidium was discovered by Robert Bunsen, Gustav Kirchhoff in 1861 at Germany. Origin of name: from the Latin word "rubidius" meaning "dark red" or "deepest red".

 
Rubidium was discovered in 1861 spectroscopically by Robert Bunsen and Gustav Kirchoff as an impurity associated with samples of the mineral lepidolite (a form of mica). The name rubidium (from the Latin "rubidus" - dark red) was coined for its bright red spectroscopic lines.

Rubidium salts were isolated by Bunsen by precipitation from spring waters - along with salts of other Group 1 elements. He was able to separate them and isolated the chloride and the carbonate. He isolated rubidium metal by reducing rubidium hydrogen tartrate with carbon.

Rubidium around us Read more »
Rubidium has no biological role but is said to stimulate the metabolism. It can accumulate ahead of potassium in muscle.

Rubidium is far too reactive to be found as the free metal in nature. It is relatively rare, although it is the 16th most abundant element in the earth\'s crust. Rubidium is present in some minerals found in North America, South Africa, Russia, and Canada. It is found in some potassium minerals (lepidolites, biotites, feldspar, carnallite), sometimes with caesium as well.

Abundances for rubidium in a number of different environments. More abundance data »
Location	ppb by weight	ppb by atoms	Links
Universe	10	0.1	Chemical elements abundance by weight in the universe on a miniature periodic table spark table
Crustal rocks	60000	14000	Chemical elements abundance by weight in the earth\'s crust on a miniature periodic table spark table
Human	4600 ppb by weight	340 atoms relative to C = 1000000	Chemical elements abundance by weight in humans on a miniature periodic table spark table
Physical properties Read more »
Density of the chemical elements on a miniature periodic table spark table Density of solid: 1532 kg m-3
Molar volume of the chemical elements on a miniature periodic table spark table Molar volume: 55.76 cm3
Thermal conductivity of the chemical elements on a miniature periodic table spark table Thermal conductivity: 58 W m‑1 K‑1
Heat properties Read more »
Melting point on a miniature periodic table spark table Melting point: 312.46 [39.31 °C (102.76 °F)] K
Boiling point on a miniature periodic table spark table Boiling point: 961 [688 °C (1270 °F)] K
Enthalpy of fusion on a miniature periodic table spark table Enthalpy of fusion: 2.19 kJ mol-1
Crystal structure Read more »
The solid state structure of rubidium is: bcc (body-centred cubic).

Rubidium: orbital properties Read more »
Rubidium atoms have 37 electrons and the shell structure is 2.8.18.8.1. The ground state electronic configuration of neutral Rubidium is [Kr].5s1 and the term symbol of Rubidium is 2S1/2.

Pauling electronegativity of the chemical elements on a miniature periodic table spark table Pauling electronegativity: 0.82 (Pauling units)
First ionization energy the chemical elements on a miniature periodic table spark table First ionisation energy: 403.0 kJ mol‑1
Second ionization energy the chemical elements on a miniature periodic table spark table Second ionisation energy: 2633 kJ mol‑1
Isolation
Isolation: rubidium would not normally be made in the laboratory as it is available commercially. All syntheses require an electrolytic step as it is so difficult to add an electron to the poorly electronegative rubidium ion Rb+.

Rubidium is not made by the same method as sodium as might have been expected. This is because the rubidium metal, once formed by electrolysis of liquid rubidium chloride (RbCl), is too soluble in the molten salt.

cathode: Rb+(l) + e- → Rb (l)
anode: Cl-(l) → 1/2Cl2 (g) + e-
Instead, it is made by the reaction of metallic sodium with hot molten rubidium chloride.

Na + RbCl ⇌ Rb + NaCl
This is an equilibrium reaction and under these conditions the rubidium is highly volatile and removed from the system in a form relatively free from sodium impurities, allowing the reaction to proceed.

Rubidium isotopes Read more »
Table. Stables isotopes of rubidium.
Isotope	Mass
/Da	Natural
abund.
(atom %)	Nuclear
spin (I)	Nuclear
magnetic
moment (μ/μN)
85Rb	84.911794 (3)	72.17 (2)	5/2	1.35303
87Rb	86.909187 (3)	27.83 (2)	3/2	2.75124

 
WebElements chemistry shop
You can buy periodic table posters, mugs, T-shirts, periodic table fridge magnets, games, molecular models, and more at the WebElements periodic table shop

WebElements poster
Details » Molymod molecular model kits
Details » Periodic table games: educational
Details » Periodic table shirts, ties and socks
Details »
 Rubidium location
 	K	Ca
 	Rb	Sr
 	Cs	Ba
Actinium
Aluminium
Aluminum
Americium
Antimony
Argon
Arsenic
Astatine
Barium
Berkelium
Beryllium
Bismuth
Bohrium
Boron
Bromine
Cadmium
Caesium
Calcium
Californium
Carbon
Cerium
Cesium
Chlorine
Chromium
Cobalt
Copernicium
Copper
Curium
Darmstadtium
Dubnium
Dysprosium
Einsteinium
Erbium
Europium
Fermium
Flerovium
Fluorine
Francium
Gadolinium
Gallium
Germanium
Gold
Hafnium
Hassium
Helium
Holmium
Hydrogen
Indium
Iodine
Iridium
Iron
Krypton
Lanthanum
Lawrencium
Lead
Lithium
Livermorium
Lutetium
Magnesium
Manganese
Meitnerium
Mendelevium
Mercury
Molybdenum
Moscovium
Neodymium
Neon
Neptunium
Nickel
Nihonium
Niobium
Nitrogen
Nobelium
Oganesson
Osmium
Oxygen
Palladium
Phosphorus
Platinum
Plutonium
Polonium
Potassium
Praseodymium
Promethium
Protactinium
Radium
Radon
Rhenium
Rhodium
Roentgenium
Rubidium
Ruthenium
Rutherfordium
Samarium
Scandium
Seaborgium
Selenium
Silicon
Silver
Sodium
Strontium
Sulfur
Sulphur
Tantalum
Technetium
Tellurium
Tennessine
Terbium
Thallium
Thorium
Thulium
Tin
Titanium
Tungsten
Uranium
Vanadium
Xenon
Ytterbium
Yttrium
Zinc
Zirconium

Custom Search
search
37	Rb
Rb	  Rb	
  Rubidium	  Rubidium
  Rubidium	  Rubidium
  Rubidio	  Rubidio
  Rubidium	  Rubídio
Description 
Rubidium
Key information
small picture of Rb
List of properties
History
Uses
Element properties
Crystal structure
icon of Rb crystal structure (space filling)
Physical properties
Thermochemistry
Atom properties
simulated Rb atomic spectrum
Electron shell properties
Atom sizes
Electronegativity
Rubidium	around us
cartogram depicting abundance of elements in the earth\'s crust
Geology
Biology
Chemistry and compounds
Compounds
Reactions of Rb
Rubidium	compound properties
Nuclear properties
Isotopes and NMR
Periodic table door poster

Periodic table door poster

The Orbitron gallery of orbitals.

QR-code link to rubidium web page


 
WebElements home
WebElements:
the periodic table on the WWW
[www.webelements.com]

WebElements logoPrintable table

Copyright 1993-2016 Mark Winter [The University of Sheffield and WebElements Ltd, UK]. All rights reserved.

This document dated: Thursday 9th March, 2017

 Pinterest
 Facebook
 Twitter
 Contact
 Chemdex
 Chemputer
 Privacy
 About
 Copyright
 Thanks...
Shop
Posters
Periodic table
fridge magnets
Molecular model kits
Molecular orbital kits
Molecular model
kit spares
Mugs and games
Educational
T-shirts
spacer
Copyright 1993-2016 Mark Winter [The University of Sheffield and WebElements Ltd, UK]. All rights reserved.'),
					'footers'			=> array(	'footer_1_anchor'	=> 'Frontpage footer',
													'footer_2_anchor'	=> 'Page footer' )	);

$t = new WordTemplate();
$t->makeWordDoc( './Testtemplate.docx' ,'./Editedtemplate.docx', $keyhash );
echo( '[<a href="'. $t->getTemplate_path(). '"> The template word document</a>].<br/>' );
echo( '[<a href="'. $t->output_path. '"> Your freshly baked word document</a>].<hr/>' );