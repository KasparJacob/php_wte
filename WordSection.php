<?php
/* 
 * Kaspar Smoolenaars <kaspar.smoolenaars@gmail.com>
 * 
 * Class that the individual sections ( header, footer, dociment ) are based on. Handles the substitutions.
 */

abstract class WordSection {
	const ANCHOR_REGEX 			= '/%%(?P<label>.+?)%%/';
	
	public function lazyStrip( $label ) {
		return preg_replace( '/<.+?>/', '', $label );
	}
	
	protected $filepath		= '';
	protected $keyhash		= array();
	
	public function __construct( $filepath ) {
		$this->filepath = $filepath;
	}
	
	public function make( $keyhash, $regex=null ){
		$this->keyhash = ( is_array( $keyhash ) ? $keyhash : array() );
		$filecontent = file_get_contents( $this->filepath );
		if( is_null( $regex ) ) {
			$regex = self::ANCHOR_REGEX;
		}
		$content = preg_replace_callback( $regex, array( $this, 'replace' ), $filecontent );
		unset( $filecontent );
		return ( file_put_contents( $this->filepath, $content ) !== false );
	}
	
	protected function replace( $matches ) {
		$label = self::lazyStrip( $matches['label'] );
		if( array_key_exists( $label, $this->keyhash ) ) {
			return $this->keyhash[$label];
		} 
		return $matches['label'];
	}
}