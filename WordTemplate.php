<?php 
require_once 'WordHeader.php';
require_once 'WordDocument.php';
require_once 'WordFooter.php';

/* 
 * Kaspar Smoolenaars <kaspar.smoolenaars@gmail.com>
 * An attempt to use word-documents as a template for generated documents.
 * 
 * Class that handles the input and output. I try to KISS.
 */

class WordTemplate {  
	protected static function backflip( $path, $add_slash = true ) {
		$path = str_replace( '\\', '/', $path );
		return $path. ( substr( $path, -1, 1 ) != '/' && $add_slash ?  '/' : '' );
	}

	public		$template_path		= '';
	public		$output_path		= '';
	protected	$temppath_exists	= false;
	protected	$template_temppath	= '';
	protected 	$zip				= null;
	
	public function makeWordDoc( $template_path, $output_path, $keyhash ) {
		$this->template_path	= $template_path;
		$this->output_path		= $output_path;
		$this->unzipIt();
		if( array_key_exists( 'headers', $keyhash ) ) {
			$this->makeHeaders( $keyhash['headers'] );
		}
		if( array_key_exists( 'document', $keyhash ) ) {
			$this->makeDocument( $keyhash['document'] );
		}
		if( array_key_exists( 'footers', $keyhash ) ) {
			$this->makeFooters( $keyhash['footers'] );
		}
		$this->zipIt();
		$this->deleteTemplate_temppath();
	}

	protected function makeHeaders( $keyhash ) {
		$return = true;
		foreach( glob( $this->getTemplate_temppath(). 'word/header*.xml' ) as $filename ) {
			$header = new WordHeader( self::backflip( $filename, false ) );
			$return &= $header->make( $keyhash );
		}
		return $return;
	}
	
	protected function makeDocument( $keyhash ) {
		$return = true;
		foreach( glob( $this->getTemplate_temppath(). 'word/document.xml' ) as $filename ) {
			$document = new WordDocument( self::backflip( $filename, false ) );
			$return &= $document->make( $keyhash );
		}
		return $return;
	}
	
	protected function makeFooters( $keyhash ) {
		$return = true;
		foreach( glob( $this->getTemplate_temppath(). 'word/footer*.xml' ) as $filename ) {
			$footer = new WordFooter( self::backflip( $filename, false ) );
			$return &= $footer->make( $keyhash );
		}
		return $return;
	}
	
	protected function zipIt( $pathname = null ) {
		if( $this->output_path ) {
			$this->output_path = self::backflip( $this->output_path, false );
			if( is_null( $this->zip ) ) {
				$this->zip = new ZipArchive();
				if( !$this->zip->open( $this->output_path, ZIPARCHIVE::CREATE ) ) {
					throw new Exception( "Cannot open output zip archive [". $this->output_path. "]." );
				}
			}
			if( is_null( $pathname ) ) {
				$pathname = $this->getTemplate_temppath();
			}
			if( file_exists( $pathname ) ) {
				$dirit = new RecursiveDirectoryIterator( $pathname, RecursiveDirectoryIterator::SKIP_DOTS );
				$fileit = new RecursiveIteratorIterator( $dirit, RecursiveIteratorIterator::CHILD_FIRST );
				foreach( $fileit as $filename ) {
					$filename = self::backflip( $filename, false );
					$filepath = str_replace( $this->getTemplate_temppath(), '', $filename );
					if( is_file( $filename ) ) {
						$this->zip->addFile( $filename, $filepath );
					} else if( is_dir( $filename ) ) {
						$this->zip->addEmptyDir( $filepath );
						$this->zipIt( $filename );
					}
				}
				if( $pathname == $this->getTemplate_temppath() ) {
					$this->zip->close();
				}
				return true;
			}
			throw new Exception( "Source folder [${pathname}] not available." );
		}
		throw new Exception( "Target path not set." );
	}
	
	protected function unzipIt() {
		if( file_exists( $this->template_path ) ) {
			$zip = new ZipArchive;
			if( $zip->open( $this->template_path ) ) {
				$zip->extractTo( $this->getTemplate_temppath() );
				$zip->close();
				return true;
			}
			throw new Exception( "Cannot open template file." );
		}
		return false;
	}
	
	public function getTemplate_path() {
		return $this->template_path;
	}
	
	protected function getTemplate_temppath() {
		if( !$this->temppath_exists ) {
			$time = microtime( true );
			$this->template_temppath = self::backflip( preg_replace( '/.docx$/','', $this->template_path ). $time );
			$this->temppath_exists = true;
		}
		return $this->template_temppath;
	}
	
	protected function makeTemplate_temppath() {
		$this->temppath_exists = mkdir( $this->getTemplate_temppath() );
		return $this->temppath_exists;
	}
	
	protected function deleteTemplate_temppath() {
		if( $this->temppath_exists ) {
			$dirit = new RecursiveDirectoryIterator( $this->getTemplate_temppath(), RecursiveDirectoryIterator::SKIP_DOTS );
			$fileit = new RecursiveIteratorIterator( $dirit, RecursiveIteratorIterator::CHILD_FIRST );
			foreach( $fileit as $file ) {
				if( $file->isDir() ){
					rmdir( $file->getRealPath() );
				} else {
					unlink( $file->getRealPath() );
				}
			}
			$this->temppath_exists = !rmdir( $this->getTemplate_temppath() );
			return true;
		}
		return false;
	}
}